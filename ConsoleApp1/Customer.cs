﻿namespace ConsoleApp1 {
    public class Customer : Person {
        private ushort idCustomer;
        private string enterpriseName;
//        private string enterpriseAddress;
        
        public Customer(){}

        public Customer(ushort idCustomer, string enterpriseName, string name, string surName, ushort age, string address) : base( name, surName, age, address){

            this.idCustomer = idCustomer;
            this.enterpriseName = enterpriseName;
        }
            
        public ushort getIdCustomer(){
            return this.idCustomer;
        }

        public string getEnterpriseName(){
            return this.enterpriseName;
        }

//        public string getEnterpriseAddress(){
//            return this.enterpriceAddress;
//        }

        public void setIdCustomer(ushort id){
            this.idCustomer = id;
        }

        public void setEnterpriseName(string enterpriseName){
            this.enterpriseName = enterpriseName;
        }

//        public void setEnterpriseAddress(string enterpriseAddress){
//            this.enterpriceAddress = enterpriseAddress;
//        }

    }
}