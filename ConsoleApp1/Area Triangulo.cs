﻿using System;

namespace ConsoleApp1 {
    
    public class Area_Triangulo {
        
        
        public int calcularAreaConLados(int bases, int altura){
            if (bases <= 0) {
                throw new NuevasExcepciones("El dato de la base ingresado es menor que 1");
            } else if (altura <= 0) {
                throw new NuevasExcepciones("El dato de la altura ingresado es menor que 1");
            }
            return ((bases * altura) / 2);
        }

        public int calcularIngresandoLados(int ladoCorto, int ladoLargo, int tercerLado){
            if (ladoCorto <= 0) {
                throw new NuevasExcepciones("El lado mas corto es menor o igual que 0");
            } else if (ladoLargo <= 0) {
                throw new NuevasExcepciones("El lado mas largo es menor o igual que 0");
            } else if (tercerLado <= 0) {
                throw new NuevasExcepciones("El tercer lado es menor o igual que 0");
            } else if ( Math.Sqrt( ( Math.Pow(ladoCorto, 2 ) ) +( Math.Pow(ladoLargo, 2) ) )  <= tercerLado) {
                throw new NuevasExcepciones("El triangulo no es cerrado");
            }

            return (ladoCorto * ladoLargo) / 2;
        }
    }
}