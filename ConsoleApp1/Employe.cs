﻿namespace ConsoleApp1 {
    public class Employe : Person {
        private ushort idEmploye;
        private string bossName;

        public Employe(){}

        public Employe(ushort idEmploye, string bossName, string name, string surName, ushort age, string address) : base( name, surName, age, address){

            this.idEmploye = idEmploye;
            this.bossName = bossName;
        }
        
        public ushort getId(){
            return this.idEmploye;
        }

        public void setId(ushort id){
            this.idEmploye = id;
        }

        public string getBossName(){
            return this.bossName;
        }

        public void setBossName(string bossName){
            this.bossName = bossName;
        }
    }
}