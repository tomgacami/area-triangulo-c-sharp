﻿namespace ConsoleApp1 {
    public class CalculoDeAristas {

        public int calculoDeAristas(int cantVertices){
            if (cantVertices == 0) {
                throw new NuevasExcepciones("El valor de vertices ingresado es cero");
            } else if (cantVertices < 0) {
                throw new NuevasExcepciones("El valor de vertices ingresado es menor a cero");
            }
            return ((cantVertices*(cantVertices-1))/2);
        }
    }
}