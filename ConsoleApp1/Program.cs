﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1 {
    class Program{
        static void Main(string[] args){


            bool esNumeroString(string entrada){
                //Funcion para controlar que lo que se ingreso sea numerico

                int iterador = 0; // variable para controlar caracter por caracter la entrada String
                char[] num = entrada.ToCharArray(); // se pasa el String de entrada a un array de chars
                int largo = entrada
                    .Length; // variable del largo del string de entrada para controlar que no se vaya de rango
                while (iterador < largo) {
                    if (Char.GetNumericValue(num[iterador]) >= 0 && Char.GetNumericValue(num[iterador]) <= 9) {
                        iterador++;
                    }
                    else {
                        return false;
                    }
                }

                return true;
            }

            Console.WriteLine("Hello World this is a prove!");
            Console.WriteLine("Hola mundo");
            const double Phi = Math.PI;
            Console.WriteLine(Phi);

            Area_Triangulo aTg = new Area_Triangulo();
            CalculoDeAristas calculoAristas = new CalculoDeAristas();
            List<Person> personas = new List<Person>();

            
            int opcion = 0, resultado = 0, opcionPrograma = 0;
            char opcionSalida = 'a';

            string opcionStg = "";
            bool esNumero = false;


            do {

                Console.WriteLine("Ingrese una opcion");
                Console.WriteLine("    1-Programa de personas");
                Console.WriteLine("    2-Programa de hallar area y aristas");
                Console.Write("Ingrese una opcion: ");

                esNumero = false;
                do {
                    opcionStg = Console.ReadLine();
                    esNumero = esNumeroString(opcionStg); // funcion que controla que sea numerica la entrada
                    if (!esNumero) {
                        Console.WriteLine("A ingresado una opción incorrecta!!!!");
                        Console.Write("Ingrese una opcion correcta: ");
                    }
                } while (!esNumero);

                opcionPrograma = Convert.ToInt32(opcionStg);
                switch (opcionPrograma) {

                    case (1):

                        Console.WriteLine("\nPROGRAMA DE PERSONAS----------\n");

                        do {
                        
                            int numGenerado, par = 0, impar = 0;

                            for (ushort e = 0; e < 5; e++) {
                                for (ushort i = 0; i < 10; i++) {
                                    numGenerado = new Random().Next(1, 2147483647);
                                    numGenerado = new Random().Next(numGenerado);
                                    //                            Console.WriteLine("{0} - {1}", numGenerado, numGenerado % 2);
                                    if (numGenerado % 2 == 0) {
                                        par++;
                                        /// Customer
                                        Customer cliente = new Customer(i, "Empresa" + i, "Tom" + i, "Garcia" + i, 0,"Gloria" + i);
                                        personas.Insert(0, cliente);
                                    }
                                    else if (numGenerado % 2 == 1) {
                                        impar++;
                                        /// Employe
                                        Employe empleado = new Employe(i, "Jefe" + i, "Empleado" + i, "Apellido" + i, 0,"Gloria" + i);
                                        personas.Insert(0, empleado);
                                    }
                                }

                                Console.WriteLine("\nPar " + par);
                                Console.WriteLine("Impar " + impar);
                                par = 0;
                                impar = 0;
                            }
                            
                            //////// DYNAMIC CAST
                            for (ArgIterator = personas. ;  < UPPER; ++) {
                                
                            }
                            Customer client = Cast(personas.)
                            
                            
                            Console.Write("\nQuiere seguir ingrresando personas? S/n ");
                            opcionSalida = Console.ReadLine()[0];

                            while (opcionSalida != 'S' && opcionSalida != 's' && opcionSalida != 'n' && opcionSalida != 'N') {
                                Console.Write("Opción no valida, vuelva a ingresar opcion: ");
                                opcionSalida = Console.ReadLine()[0];
                            }
                        } while (opcionSalida == 'S' || opcionSalida == 's'); //    FIN PROGRAMA DE PERSONAS 

                        break;

                    
                    
                    case (2):
                        Console.WriteLine("\nBUENAS BUENAS JUGADOR");
                        do {
                            Console.WriteLine("Ingrese la opcion");
                            Console.WriteLine("    1-Para ingresar base y altura");
                            Console.WriteLine("    2-Para ingresar la medida de los lados del triangulo");
                            Console.WriteLine("    3-Calcular cantidad de aristas");
                            Console.WriteLine("    4-Calcular cantidad de aristas de 1 vertice a un numero\n");
                            Console.Write("Ingrese opcion: ");

                            opcionStg = "";
                            esNumero = false;

                            do {

                                opcionStg = Console.ReadLine();
                                esNumero = esNumeroString(opcionStg); // funcion que controla que sea numerica la entrada
                                if (!esNumero) {
                                    Console.WriteLine("A ingresado una opción incorrecta!!!!");
                                    Console.Write("Ingrese una opcion correcta: ");
                                }
                            } while (!esNumero);

                            opcion = Convert.ToInt32(opcionStg);
                            switch (opcion) {

                                case 1:
                                    int altura = 0;
                                    int bases = 0;
                                    Console.WriteLine("Ingrese la medida de los lados del triangulo");
                                    Console.Write("    Altura: ");

                                    altura = Convert.ToInt32(Console.ReadLine());
                                    Console.Write("    Base: ");
                                    bases = Convert.ToInt32(Console.ReadLine());

                                    try {
                                        resultado = aTg.calcularAreaConLados(bases, altura);
                                        Console.WriteLine("El area del triangulo es: " + resultado);
                                    }
                                    catch (Exception e) {
                                        Console.WriteLine(e.Message);
                                    }

                                    break;

                                case 2:
                                    int ladoLargo = 0, ladoCorto = 0, tercerLado = 0;
                                    Console.WriteLine("Ingresar las medidas de los lados: ");
                                    Console.Write("    Ingrese el lado mas largo: ");
                                    ladoLargo = Convert.ToInt32(Console.ReadLine());
                                    Console.Write("    Ingrese el lado mas corto: ");
                                    ladoCorto = Convert.ToInt32(Console.ReadLine());
                                    Console.Write("    Ingresar el tercer lado: ");
                                    tercerLado = Convert.ToInt32(Console.ReadLine());

                                    try {
                                        resultado = aTg.calcularIngresandoLados(ladoCorto, ladoLargo, tercerLado);
                                        Console.WriteLine("El area del triangulo es: " + resultado);
                                    }
                                    catch (Exception e) {
                                        Console.WriteLine(e.Message);
                                    }

                                    break;

                                case 3:
                                    int cantVertices;
                                    Console.Write("Ingrese cantidad de vertices: ");
                                    cantVertices = Convert.ToInt32(Console.ReadLine());

                                    try {
                                        resultado = calculoAristas.calculoDeAristas(cantVertices);
                                        Console.WriteLine("La cantidad de aristas es: " + resultado);
                                    }
                                    catch (Exception e) {
                                        Console.WriteLine(e.Message);
                                    }

                                    break;

                                case 4:
                                    int cantVerticeMax, resultadoAnterior = 0;
                                    Console.Write("Ingrese cantidad de vertices: ");
                                    cantVerticeMax = Convert.ToInt32(Console.ReadLine());

                                    if (cantVerticeMax > 0) {

                                        Console.Write("\n");
                                        for (int iterator = 1; iterator <= cantVerticeMax; iterator++) {

                                            try {
                                                resultado = calculoAristas.calculoDeAristas((iterator));
                                                Console.WriteLine(iterator + "v --> " + resultado +
                                                                  "a, diferencia con el anterior: " +
                                                                  (resultado - resultadoAnterior));
                                                resultadoAnterior = resultado;
                                            }
                                            catch (Exception e) {
                                                Console.WriteLine(e.Message);
                                            }
                                        }
                                    }
                                    else if (cantVerticeMax == 0) {
                                        Console.WriteLine("El numero ingresado es cero. Ingrese un numero mayor");
                                    }
                                    else if (cantVerticeMax < 0) {
                                        Console.WriteLine(
                                            "El numero ingresado es menor que cero. Ingrese un numero mayor a cero");
                                    }

                                    break;

                                default:
                                    Console.WriteLine("\nNo hay una opcion disponible para el valor ingresado\n");
                                    break;
                            }

                            Console.Write("\nQuiere seguir calculando? S/n ");
                            opcionSalida = Console.ReadLine()[0];

                            while (opcionSalida != 'S' && opcionSalida != 's' && opcionSalida != 'n' && opcionSalida != 'N') {
                                Console.Write("Opción no valida, vuelva a ingresar opcion: ");
                                opcionSalida = Console.ReadLine()[0];
                            }

                        } while (opcionSalida == 'S' || opcionSalida == 's'); //    FIN PROGRAMA CALCULO AREA TRIANGULO Y ARISTAS 

                        break;
                    
                    default:
                        Console.WriteLine("\nNo hay una opcion disponible para el valor ingresado\n");
                    break;
                }

                Console.Write("\nQuiere elegir otro programa? S/n ");
                opcionSalida = Console.ReadLine()[0];

                while (opcionSalida != 'S' && opcionSalida != 's' && opcionSalida != 'n' && opcionSalida != 'N') {
                    Console.Write("Opción no valida, vuelva a ingresar opcion: ");
                    opcionSalida = Console.ReadLine()[0];
                }
                
            } while ( opcionSalida == 'S' || opcionSalida == 's' );    //    FIN DEL PROGRAMA TOTAL

            Console.WriteLine("\nFIN DEL PROGRAMA!!!");
        }
    }
}