﻿using System;

//namespace ConsoleApp1 {
//    public class NuevasExcepciones : Exception {
//        public NuevasExcepciones(string mensaje){
//        }
//    }
//}

[Serializable]
public class NuevasExcepciones : Exception {
    
    public NuevasExcepciones(){}

    public NuevasExcepciones(string mensaje) : base(String.Format(mensaje)){
        
    }


}