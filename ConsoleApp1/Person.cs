﻿using System.Security.Cryptography;

namespace ConsoleApp1 {
    public class Person {

        private string  name;
        private string surName;
        private ushort age;
        private string address;

        public Person(){}

        public Person(string name, string surname, ushort age, string address){
            this.name = name;
            this.surName = surname;
            this.age = age;
            this.address = address;
        }

        public string getName(){
            return this.name;
        }

        public string getSurName(){
            return this.surName;
        }
        public ushort getAge(){
            return this.age;
        }

        public string getAddress(){
            return this.address;
        }

        public void setAddress(string address){
            this.address = address;
        }

        public void setName(string name){
            this.name = name;
        }

        public void setSurName(string surName){
            this.surName = surName;
        }

        public void setAge(ushort age){
            this.age = age;
        }
    }
}